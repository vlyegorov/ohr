package edu.karazin.GenericTestFramework.util.testng;

import edu.karazin.GenericTestFramework.util.PropertiesContext;
import org.testng.IAlterSuiteListener;
import org.testng.xml.XmlSuite;

import java.util.List;

public class ThreadCountTransformer implements IAlterSuiteListener {
    /**
     * Override suit parametrs to inject variable thread count
     *
     * @param suites
     */
    @Override
    public void alter(List<XmlSuite> suites) {
        XmlSuite suite = suites.get(0);
        suite.setVerbose(10);
        suite.setParallel(XmlSuite.ParallelMode.TESTS);
        suite.setThreadCount(Integer.parseInt(PropertiesContext.getInstance().getProperty("threadcount")));
        Reporter.log("Setting thread count for suit: " + suite.getThreadCount(), 1, true);
    }
}
