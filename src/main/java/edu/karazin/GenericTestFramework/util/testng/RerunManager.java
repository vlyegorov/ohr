package edu.karazin.GenericTestFramework.util.testng;

import edu.karazin.GenericTestFramework.util.PropertiesContext;
import org.openqa.selenium.WebDriverException;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RerunManager implements IRetryAnalyzer {

    private int retryCount = 0;
    private int maxRetryCount = Integer.parseInt(PropertiesContext.getInstance().getProperty("maxRetryCount"));

    /**
     * This listener triggers in the end of the test and analise throwable, if triggered original test would be marked as skipped.
     *
     * @param result
     * @return
     */
    @Override
    public boolean retry(ITestResult result) {
        if (maxRetryCount != 0) {
            if (!result.isSuccess() && result.getThrowable() instanceof WebDriverException | result.getThrowable() instanceof AssertionError) {
                if (retryCount < maxRetryCount) {
                    Reporter.log("Retrying test " + result.getTestName(), 1, true);
                    retryCount++;
                    result.setStatus(ITestResult.SUCCESS);
                    String message = Thread.currentThread().getName() + ": Error in " + result.getName() + " Retrying "
                            + (maxRetryCount + 1 - retryCount) + " more times";
                    Reporter.log("" + message, 1, true);
                    //System.out.println("##teamcity[testIgnored name='" + BaseWebDriverTest.escapeString(result.getTestName()) + "' message='" + BaseWebDriverTest.escapeString(message) + "']");
                    return true;
                } else {
                    result.setStatus(ITestResult.FAILURE);
                }
            }
        }
        return false;
    }
}
