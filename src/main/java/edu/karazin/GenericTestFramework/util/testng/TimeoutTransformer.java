package edu.karazin.GenericTestFramework.util.testng;

import edu.karazin.GenericTestFramework.util.PropertiesContext;
import org.testng.IAnnotationTransformer;
import org.testng.annotations.ITestAnnotation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class TimeoutTransformer implements IAnnotationTransformer {

    /**
     * Add timeout and rerun manager to each test annotation
     *
     * @param annotation
     * @param testClass
     * @param testConstructor
     * @param testMethod
     */
    @Override
    public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
        int timeout = Integer.parseInt(PropertiesContext.getInstance().getProperty("test.timeout")) * 60 * 1000;
        annotation.setInvocationTimeOut(timeout);
        annotation.setTimeOut(timeout);
        annotation.setRetryAnalyzer(RerunManager.class);
    }
}
