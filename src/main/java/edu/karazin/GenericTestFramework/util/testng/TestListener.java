package edu.karazin.GenericTestFramework.util.testng;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import edu.karazin.GenericTestFramework.tests.BaseTest;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.util.logging.Level;

/**
 * Listens to test and collecting results on failure
 */
public class TestListener extends TestListenerAdapter {
    private static final String SNAPSHOT_HEIGHT = "480px";
    private static final String SNAPSHOT_WIDTH = "848px";

    WebDriver driver;

    @Override
    public void onTestSuccess(ITestResult tr) {
        Reporter.generateReport(tr);
    }

    @Override
    public void onTestSkipped(ITestResult tr) {
        Reporter.generateReport(tr);
    }

    @Override
    /**
     * Collect logs after failure
     */
    public void onTestFailure(ITestResult result) {
        Object currentClass = result.getInstance();
        try {
            driver = ((BaseTest) currentClass).getDriver();
            for (LogEntry entry : driver.manage().logs().get(LogType.BROWSER).filter(Level.SEVERE)) {
                Reporter.log("***** Severe JS error " + entry.getMessage() + " *****", 1, true);
            }
            for (LogEntry entry : driver.manage().logs().get(LogType.BROWSER).filter(Level.WARNING)) {
                Reporter.log("***** Warning JS error " + entry.getMessage() + " *****", 1, true);
            }
        } catch (Exception e) {
            Reporter.log("Couldn't retrieve browser logs", 1, true);
        }

        Reporter.log("***** Error " + result.getName() + " test has failed *****", 1);
        Reporter.log("***** Error message: " + result.getThrowable().getMessage() + " *****", 1);
        Reporter.log("***** Caused by: " + result.getThrowable().getCause() + " *****", 1);
        String methodName = result.getName().trim();
        String filePath = Reporter.takeScreenShot(methodName, driver);
        Reporter.log("" + filePath, 1, true);
        Reporter.log("<img src=\"" + filePath
                + "\" height=\"" + SNAPSHOT_HEIGHT
                + "\" width=\"" + SNAPSHOT_WIDTH + "\">", 1, true);
        Reporter.generateReport(result);
    }
}
