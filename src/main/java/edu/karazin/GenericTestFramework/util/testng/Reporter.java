package edu.karazin.GenericTestFramework.util.testng;

import edu.karazin.GenericTestFramework.util.SoftAssert;
import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import edu.karazin.GenericTestFramework.util.PropertiesContext;
import org.testng.ITestResult;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Reporter {

    // repository/download/{env.BUILD_ID}/{env.BUILD_NUMBER}/{project_dir}/{report_dir}/{snapshots_path}
    private static final String TC_RESOURCE_PATTERN = "/repository/download/%s/%s/%s/%s/%s";
    // {project_dir}/{report_dir}/{snapshots_path}
    private static final String LOCAL_RESOURCE_PATTERN = "%s/%s/%s";

    private static final String TC_BUILD_ID_PARAM = "BUILD_ID";
    private static final String TC_BUILD_NUMBER_PARAM = "BUILD_NUMBER";
    private static final String SNAPSHOT_NAME_PATTERN = "%s/%s---%s-%d.png";

    public static String buildURL(String relativeFilePath) {
        String projectDir = PropertiesContext.getInstance().getProperty(SoftAssert.PROJECT_DIRECTORY);
        String reportDir = PropertiesContext.getInstance().getProperty(SoftAssert.SNAPSHOT_DIRECTORY);
        Map<String, String> envVars = System.getenv();

        if (envVars.containsKey(TC_BUILD_ID_PARAM) && envVars.containsKey(TC_BUILD_NUMBER_PARAM)) {
            return String.format(TC_RESOURCE_PATTERN,
                    envVars.get(TC_BUILD_ID_PARAM),
                    envVars.get(TC_BUILD_NUMBER_PARAM),
                    projectDir,
                    reportDir,
                    relativeFilePath);
        } else {
            return String.format(LOCAL_RESOURCE_PATTERN,
                    projectDir,
                    reportDir,
                    relativeFilePath);
        }
    }

    public static void log(String s, int level, boolean logToStandardOut) {
        s = "Level: " + level + " at: " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("hh:mm:ss dd/MM/yyyy")) + " " + s;
        org.testng.Reporter.log(s, level, logToStandardOut);
    }

    public static void log(String s, int level) {
        log(s, level, true);
    }

    public static synchronized List<String> getOutput(ITestResult tr) {
        return org.testng.Reporter.getOutput(tr);
    }

    @Attachment(value = "Screenshot", type = "image/png")
    public static byte[] takeScreenShot(WebDriver driver) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    /**
     * @return Relative path to snapshot
     */

    public static String takeScreenShot(String methodName, WebDriver driver) {
        String reportDir = PropertiesContext.getInstance().getProperty(SoftAssert.SNAPSHOT_DIRECTORY);
        String filePath = String.format(SNAPSHOT_NAME_PATTERN,
                SoftAssert.SNAPSHOTS,
                methodName,
                SoftAssert.format.format(new Date()),
                (int) (Math.random() * 9999));

        //The below method will save the screen shot in d drive with test method name
        try {
            File dstFile = new File(reportDir, filePath);
            dstFile.getParentFile().mkdirs();
            FileUtils.writeByteArrayToFile(dstFile, takeScreenShot(driver));
            try (InputStream is = new FileInputStream(dstFile)) {
                Allure.addAttachment(filePath, is);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Reporter.buildURL(filePath);
    }


    @Attachment()
    public static String generateReport(ITestResult result) {
        List<String> list = Reporter.getOutput(result);
        String reportDir = PropertiesContext.getInstance().getProperty(SoftAssert.SNAPSHOT_DIRECTORY);
        String filePath = String.format("%s/%s.html",
                "reports",
                result.getName() + "-" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("hh-mm dd-MM-yyyy")));
        StringBuilder html = new StringBuilder();
        html.append("<!doctype html>\n");
        html.append("<html lang='en'>\n");

        html.append("<head>\n");
        html.append("<meta charset='utf-8'>\n");
        html.append("<title>" + result.getName() + "</title>\n");
        html.append("</head>\n\n");

        html.append("<body>\n");
        html.append("<h1>" + result.getName() + "</h1>\n");
        // Make a list in HTML
        html.append("<ul>\n");
        // Loop the list of reports passed as argument.
        for (String report : list) {
            html.append("<li><code>" + report + "</code></li>\n");
        }
        html.append("</ul>\n");
        html.append("</body>\n\n");

        html.append("</html>");
        try {
            File dstFile = new File(reportDir, filePath);
            dstFile.getParentFile().mkdirs();
            FileUtils.writeStringToFile(new File(reportDir, filePath), html.toString(), "utf-8");
            try (InputStream is = new FileInputStream(dstFile)) {
                Allure.addAttachment(result.getName(), is);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return html.toString();
    }
}
