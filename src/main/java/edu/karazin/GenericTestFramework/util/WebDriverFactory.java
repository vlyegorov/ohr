package edu.karazin.GenericTestFramework.util;

import io.github.bonigarcia.wdm.Architecture;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.EdgeDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;
import io.qameta.allure.Attachment;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Reporter;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;


public class WebDriverFactory {

    public static final int DEFAULT_TIMEOUT = 15;
    public static final int DEFAULT_POLLING_DELAY = 5;
    String path = System.getProperty("user.dir");
    private WebDriver driver;
    private String browser = PropertiesContext.getInstance().getProperty("browser");

    public WebDriver getDriver() {
        if (driver == null & browser.equals("gridchrome")) {
            driver = getGridChrome();
        } else if (driver == null & browser.equals("chrome")) {
            driver = getStandaloneChrome();
        } else if (driver == null & browser.equals("ie")) {
            driver = getStandaloneIE();
        } else if (driver == null & browser.equals("gridie")) {
            driver = getGridIE();
        } else if (driver == null & browser.equals("gridedge")) {
            driver = getGridEdge();
        } else if (driver == null & browser.equals("edge")) {
            driver = getStandaloneEdge();
        }
        try {
            driver.manage().timeouts().implicitlyWait(DEFAULT_TIMEOUT, TimeUnit.SECONDS).pageLoadTimeout(DEFAULT_TIMEOUT * 4, TimeUnit.SECONDS).setScriptTimeout(DEFAULT_TIMEOUT * 4, TimeUnit.SECONDS);
        } catch (NullPointerException e) {
            Reporter.log("Something went wrong on setting default timeout");
            Reporter.log(e.toString());
        }
        return driver;
    }

    public WebDriver getGridChrome() {
        Reporter.log("Getting Chrome Grid driver", 1, true);
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        WebDriver driver = null;
        try {
            driver = new RemoteWebDriver(new URL(PropertiesContext.getInstance().getProperty("grid.hub.url")), setChromeCaps(cap, PropertiesContext.getInstance().getProperty("network.file.dir")));
            ((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
        } catch (MalformedURLException e) {
            Reporter.log("Something went wrong on setting grid driver");
        }
        logRemoteDriverInfo((RemoteWebDriver) driver);
        String session = ((RemoteWebDriver) driver).getSessionId().toString();
        Reporter.log("Session ID " + session, 1, true);
        return driver;
    }

    public WebDriver getGridIE() {
        Reporter.log("Getting IE Grid driver", 1, true);
        DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
        WebDriver driver = null;
        try {
            driver = new RemoteWebDriver(new URL(PropertiesContext.getInstance().getProperty("grid.hub.url")), setIeCaps(cap, PropertiesContext.getInstance().getProperty("network.file.dir")));
            ((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
        } catch (MalformedURLException e) {
            Reporter.log("Something went wrong on setting grid driver");
        }
        return driver;
    }

    public WebDriver getGridEdge() {
        Reporter.log("Getting Edge Grid driver", 1, true);
        DesiredCapabilities cap = DesiredCapabilities.edge();
        cap.setBrowserName("MicrosoftEdge");
        cap.setPlatform(Platform.WIN10);
        WebDriver driver = null;
        try {
            driver = new RemoteWebDriver(new URL(PropertiesContext.getInstance().getProperty("grid.hub.url")), setIeCaps(cap, PropertiesContext.getInstance().getProperty("network.file.dir")));
            ((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
        } catch (MalformedURLException e) {
            Reporter.log("Something went wrong on setting grid driver");
        }
        return driver;
    }

    @Attachment(value = "Video link", type = "text/html")
    public String logRemoteDriverInfo(RemoteWebDriver driver) {
        if (driver != null) {
            String platformName = driver.getCapabilities().getPlatform().name();
            Reporter.log("Starting driver on " + platformName, 1, true);

            String driverVersion = driver.getCapabilities().getVersion();
            Reporter.log("Driver version " + driverVersion, 1, true);

            String session = driver.getSessionId().toString();
            Reporter.log("Session ID " + session, 1, true);
            String boxName = null;
            try {
                boxName = (String) driver.getCapabilities().getCapability("boxName");
            } catch (Exception ignore) {

            }
            if (boxName == null) {
                boxName = "***** Default Video Server *****";
            }

            Reporter.log("Box name " + boxName, 1, true);
            String videoLink = "http://" + boxName + ":3000/download_video/" + session + ".mp4";
            Reporter.log("<video width=\"320\" height=\"240\" controls>", 1, true);
            Reporter.log("<source src=\"" + videoLink + "\" type=\"video/mp4\">", 1, true);
            Reporter.log("</video>", 1, true);
            return "<a href=\"" + videoLink + "\">Test Video here</a>";
        } else {
            return "";
        }
    }

    public WebDriver getStandaloneChrome() {
        ChromeDriverManager.getInstance().setup();
        Reporter.log("Getting Chrome driver " + ChromeDriverManager.getInstance().getDownloadedVersion(), 1, true);
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        WebDriver driver = new ChromeDriver(setChromeCaps(cap, path + "/downloadedFiles"));
        driver.manage().window().setSize(new Dimension(1920, 1080));
        // print size of window
        Dimension ws = driver.manage().window().getSize();
        Logger.getAnonymousLogger().log(Level.INFO,
                String.format("Size of initialized window: w=%d, h=%d", ws.getWidth(), ws.getHeight()));
        return driver;
    }

    public WebDriver getStandaloneIE() {
        InternetExplorerDriverManager.getInstance().architecture(Architecture.X32).setup();
        Reporter.log("Getting IE driver " + InternetExplorerDriverManager.getInstance().getDownloadedVersion(), 1, true);
        DesiredCapabilities cap = new DesiredCapabilities();
        WebDriver driver = new InternetExplorerDriver(setIeCaps(cap, path + "/downloadedFiles"));
        return driver;
    }

    public WebDriver getStandaloneEdge() {
        EdgeDriverManager.getInstance().setup();
        Reporter.log("Getting Edge driver " + EdgeDriverManager.getInstance().getDownloadedVersion(), 1, true);
        DesiredCapabilities cap = new DesiredCapabilities();
        WebDriver driver = new EdgeDriver(setIeCaps(cap, path + "/downloadedFiles"));
        return driver;
    }

    private DesiredCapabilities setIeCaps(DesiredCapabilities cap, String downloadFilepath) {
        cap.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
        cap.setCapability(InternetExplorerDriver.NATIVE_EVENTS, true);
        cap.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
        cap.setCapability("elementScrollBehavior", 1);
        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, false);
        cap.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
        return cap;
    }

    private DesiredCapabilities setChromeCaps(DesiredCapabilities cap, String downloadFilepath) {
        ChromeOptions options = new ChromeOptions();

        options.addArguments("--disable-extensions");
        options.addArguments("disable-infobars");
        options.addArguments("start-maximized");
        options.addArguments("test-type");
        options.addArguments("--js-flags=--expose-gc");
        options.addArguments("--enable-precise-memory-info");
        options.addArguments("--disable-popup-blocking");
        options.addArguments("--disable-default-apps");
        options.addArguments("test-type=browser");
        options.addArguments("disable-infobars");

        Reporter.log("Download File Path: " + downloadFilepath, 10, true);
        File directory = new File(String.valueOf(downloadFilepath));
        if (!directory.exists())
            directory.mkdir();
        HashMap<String, Object> chromePrefs = new LinkedHashMap<>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", downloadFilepath);
        chromePrefs.put("forceDevToolsScreenshot", Boolean.TRUE);
        HashMap<String, Object> chromeOptionsMap = new LinkedHashMap<>();
        options.setExperimentalOption("prefs", chromePrefs);

        cap.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        cap.setCapability(ChromeOptions.CAPABILITY, options);
        cap.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
        return cap;
    }


}
