package edu.karazin.GenericTestFramework.util;

import com.google.common.base.Function;
import org.openqa.selenium.*;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.Serializable;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class ByJquery extends By implements Serializable {
    private final String query;

    public ByJquery(String query) {
        checkNotNull(query, "Cannot find elements with a null JQuery expression.");
        this.query = query;
    }

    private static WebDriver getWebDriverFromSearchContext(SearchContext context) {
        if (context instanceof WebDriver) {
            return (WebDriver) context;
        }
        if (context instanceof WrapsDriver) {
            return ((WrapsDriver) context).getWrappedDriver();
        }
        throw new IllegalStateException("Can't access a WebDriver instance from the current search context.");
    }

    private static boolean isJQueryInThisPage(WebDriver driver) {
        final JavascriptExecutor js = (JavascriptExecutor) driver;
        return (Boolean) js.executeScript("return (typeof jQuery != \"undefined\")");
    }

    private static void injectJQuery(WebDriver driver) {
        final JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("dollar = null; if(typeof jQuery == \"undefined\") {" +
                "if(typeof $ != \"undefined\") {dollar = $;}" +
                " var headID = document.getElementsByTagName(\"head\")[0];" +
                "var newScript = document.createElement('script');" +
                "newScript.type = 'text/javascript';" +
                "newScript.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js';" +
                "headID.appendChild(newScript);}");

//        js.executeScript("dollar = null;var jq = document.createElement('script');" +
//                "jq.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'; " +
//                "document.getElementsByTagName('head')[0].appendChild(jq);");

        WebDriverWait wait = new WebDriverWait(driver, 30);
        System.out.println("... Injecting jQuery ...");
        Function<WebDriver, Boolean> jQueryAvailable = new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver input) {
                return (Boolean) js.executeScript("return (typeof jQuery != \"undefined\")");
            }
        };
        try {
            wait.until(jQueryAvailable);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //restore saved '$' if any were saved
        js.executeScript("if(dollar != null) {$ = dollar}");
    }

    @Override
    public List<WebElement> findElements(SearchContext context) {
        WebDriver driver = getWebDriverFromSearchContext(context);

        if (!isJQueryInThisPage(driver)) {
            injectJQuery(driver);
        }

        return new ByJavaScript("return $(\"" + query + "\")").findElements(context);
    }

    @Override
    public String toString() {
        return "By.jQuery: \"$(" + query + ")\"";
    }
}


