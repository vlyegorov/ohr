package edu.karazin.GenericTestFramework.util;

import edu.karazin.GenericTestFramework.util.testng.Reporter;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Helper extends WebDriverWait {

    private static Hashtable<WebDriver, Helper> helpersPerDriver = new Hashtable<>();
    private WebDriver driver;

    private Helper(WebDriver driver) {
        super(driver, WebDriverFactory.DEFAULT_TIMEOUT);
        this.driver = driver;
    }

    public static synchronized Helper getInstance(WebDriver driver) {
        if (driver != null && helpersPerDriver.containsKey(driver)) {
            return helpersPerDriver.get(driver);
        } else if (driver != null) {
            Helper newHelper = new Helper(driver);
            newHelper.ignoring(StaleElementReferenceException.class);//Add some annoying exceptions here
            helpersPerDriver.put(driver, newHelper);
            return newHelper;
        }
        throw new WebDriverException("No driver was found");
    }

    /**
     * `
     * In general use of this is not welcomed, use mustWaitForLoadToComplete
     */
    public void suspend(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void waitUntilJSReady() {
        until((ExpectedCondition<Boolean>) input -> {
            Object response = ((JavascriptExecutor) driver).executeScript("return document.readyState;");
            return response != null && response.toString().equals("complete");
        });
    }

    public Helper mustWaitForLoadToComplete() {
        try {
            shrinkScreen();
            waitUntilJSReady();
        } catch (TimeoutException e) {
            Reporter.log("request counter did not became 0 after timeout", 10, true);
        }
        return this;
    }


    /**
     * Scroll to element
     *
     * @param by
     * @return
     */
    public WebElement scrollTo(By by) {
        WebElement element = driver.findElement(by);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        return element;
    }

    public boolean checkIfElementExists(By by) {
        try {
            driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
            long t = (long) 1;
            new WebDriverWait(driver, t).until(ExpectedConditions.visibilityOfElementLocated(by));
            return true;
        } catch (WebDriverException e) {
            Reporter.log("Element is not exists: " + by.toString(), 5);
            return false;
        } finally {
            driver.manage().timeouts().implicitlyWait(WebDriverFactory.DEFAULT_TIMEOUT, TimeUnit.SECONDS);
        }
    }

    public WebElement untilElementVisible(By by) {
        return this.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public WebElement untilElementClickable(String xpath) {
        return this.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
    }

    /**
     * Returns text of element or null if there are no such element
     *
     * @param by Text to extract
     * @return
     */
    public String getTextSafely(By by) {
        try {
            if (this.checkIfElementExists(by)) {
                return untilElementVisible(by).getText();
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Method for fields with per simbol validation (date input)
     *
     * @param by
     * @param keys
     */
    public void sendKeysOneByOne(By by, String keys) {
        for (char key : keys.toCharArray()) {
            untilElementVisible(by).sendKeys(String.valueOf(key));
        }
    }

    /**
     * Set text or remove it if null
     *
     * @param by   input selector
     * @param text text to set
     */
    public void clearAndSetText(By by, String text) {
        untilElementVisible(by).clear();
        if (text != null) {
            untilElementVisible(by).sendKeys(text);
        }
    }

    /**
     * Magical method needed to refresh UI without refreshing page
     */
    public void shrinkScreen() {
        Dimension full = driver.manage().window().getSize();
        Dimension shrinked = new Dimension(full.getWidth() - 1, full.getHeight() - 1);
        driver.manage().window().setSize(shrinked);
        driver.manage().window().setSize(full);
    }

    public List<String> getValidationMessages() {
        try {
            return until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//*[contains(@class,'ui-form-field-error-message')]"))).stream().map(i -> i.getAttribute("textContent")).collect(Collectors.toList());
        } catch (WebDriverException e) {
            return new ArrayList<>();
        }
    }

    public void hover(WebDriver driver, By by) {
        Actions actions = new Actions(driver);
        actions.moveToElement(untilElementVisible(by)).build().perform();
    }


}
