package edu.karazin.GenericTestFramework.pages.orangehrm;

import edu.karazin.GenericTestFramework.data.Users;
import edu.karazin.GenericTestFramework.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AddSystemUsersPage extends AbstractPage {
    public AddSystemUsersPage(WebDriver driver) {
        super(driver);
    }

    public AddSystemUsersPage addRandomUser() {
        helper.clearAndSetText(By.id("systemUser_employeeName_empName"), "Fiona Grace");
        helper.clearAndSetText(By.id("systemUser_userName"), Users.LELOUCH.getName());
        helper.clearAndSetText(By.id("systemUser_password"), Users.LELOUCH.getPassword());
        helper.clearAndSetText(By.id("systemUser_confirmPassword"), Users.LELOUCH.getPassword());
        helper.untilElementVisible(By.id("btnSave")).click();
        return this;
    }

}


