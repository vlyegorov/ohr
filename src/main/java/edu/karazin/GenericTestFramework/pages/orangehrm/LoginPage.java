package edu.karazin.GenericTestFramework.pages.orangehrm;

import edu.karazin.GenericTestFramework.pages.AbstractPage;
import edu.karazin.GenericTestFramework.util.ByJquery;
import org.openqa.selenium.WebDriver;

public class LoginPage extends AbstractPage {
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage getToLoginPage() {
        navigate();
        return this;
    }

    public DashboardPage authenticate(String name, String password) {
        helper.clearAndSetText(new ByJquery("#txtUsername"), name);
        helper.clearAndSetText(new ByJquery("#txtPassword"), password);
        helper.untilElementVisible(new ByJquery("#btnLogin")).click();
        return new DashboardPage(driver);
    }

}


