package edu.karazin.GenericTestFramework.pages.orangehrm;

import edu.karazin.GenericTestFramework.pages.AbstractPage;
import edu.karazin.GenericTestFramework.util.ByJquery;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DashboardPage extends AbstractPage {
    public DashboardPage(WebDriver driver) {
        super(driver);
    }

    public ViewSystemUsersPage getToUsers() {
        helper.hover(driver, By.id("menu_admin_viewAdminModule"));
        helper.hover(driver, By.id("menu_admin_UserManagement"));
        helper.hover(driver, By.id("menu_admin_viewSystemUsers"));
        helper.untilElementVisible(By.id("menu_admin_viewSystemUsers")).click();
        return new ViewSystemUsersPage(driver);
    }

    public void logout() {
        helper.untilElementVisible(By.id("welcome")).click();
        helper.untilElementVisible(new ByJquery("a:contains(Logout)")).click();
    }


}
