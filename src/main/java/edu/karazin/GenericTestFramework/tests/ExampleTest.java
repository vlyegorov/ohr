package edu.karazin.GenericTestFramework.tests;

import edu.karazin.GenericTestFramework.data.Users;
import edu.karazin.GenericTestFramework.pages.orangehrm.DashboardPage;
import edu.karazin.GenericTestFramework.pages.orangehrm.LoginPage;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class ExampleTest extends BaseTest {

    @Test
    public void createNewUser() {
        LoginPage loginPage = new LoginPage(driver).getToLoginPage();
        DashboardPage dashboardPage = loginPage.authenticate(Users.ADMIN.getName(), Users.ADMIN.getPassword());
        dashboardPage.getToUsers().clickAddUser().addRandomUser();
        dashboardPage.logout();
        loginPage.authenticate(Users.LELOUCH.getName(), Users.LELOUCH.getPassword());
        softAssert.assertEquals(wait.getTextSafely(By.id("welcome")), "Welcome Fiona");
    }
}
