package edu.karazin.GenericTestFramework.tests;

import edu.karazin.GenericTestFramework.util.Helper;
import edu.karazin.GenericTestFramework.util.PropertiesContext;
import edu.karazin.GenericTestFramework.util.SoftAssert;
import edu.karazin.GenericTestFramework.util.WebDriverFactory;
import edu.karazin.GenericTestFramework.util.testng.TestListener;
import edu.karazin.GenericTestFramework.util.testng.TestMethodListener;
import org.openqa.selenium.NoSuchSessionException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.*;

import java.util.logging.Level;

@Listeners({TestMethodListener.class, TestListener.class})
public class BaseTest {


    public SoftAssert softAssert;
    public PropertiesContext context;
    public WebDriver driver;
    public Helper wait;

    public Long suitStart, suitStop = 0L;

    public SoftAssert getSoftAssert() {
        return softAssert;
    }

    public WebDriver getDriver() {
        return driver;
    }

    @BeforeSuite
    public void beforeSuite() {
        suitStart = System.currentTimeMillis();
        Reporter.log("<br>Setting server: " + PropertiesContext.getInstance().getProperty("envurl"), true);
        Reporter.log("<br>Setting browser: " + PropertiesContext.getInstance().getProperty("browser"), true);
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod(final ITestContext testContext) {
        Reporter.log("<br>Starting test " + testContext.getName(), true);
        context = PropertiesContext.getInstance();
        WebDriverFactory factory = new WebDriverFactory(); //Init factory to use 1 webdriver
        driver = factory.getDriver(); //Init webdriver
        wait = Helper.getInstance(driver); //Init helper
        softAssert = SoftAssert.getInstance(driver);
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod(final ITestContext testContext, ITestResult result) {
        if (driver != null) {
            driver.quit();
        }
        try {
            for (LogEntry entry : driver.manage().logs().get(LogType.BROWSER).filter(Level.SEVERE)) {
                Reporter.log("<br>***** Severe JS error " + entry.getMessage() + " *****", true);
            }
            for (LogEntry entry : driver.manage().logs().get(LogType.BROWSER).filter(Level.WARNING)) {
                Reporter.log("<br>***** Warning JS error " + entry.getMessage() + " *****", true);
            }
        } catch (NoSuchSessionException ignore) {

        }
        Reporter.log("<br>Completed test method " + result.getName(), true);
        Reporter.log("<br>****************************************************", true);
        Reporter.log("<br>Time taken by method " + result.getName() + ": " + (result.getEndMillis() - result.getStartMillis()) / 1000 + "sec", true);
        Reporter.log("<br>****************************************************", true);
    }

    @AfterSuite
    public void afterSuite(final ITestContext ctx) {
        if (driver != null) {
            driver.quit();
        }
        String suitName = ctx.getSuite().getName();
        suitStop = System.currentTimeMillis();
        Long elapsedTime = (suitStop - suitStart) / 1000;
        Reporter.log("<br>Suit  " + suitName + " took " + elapsedTime + " seconds", true);
    }

}
